
import { SkFormImpl }  from '../../sk-form/src/impl/sk-form-impl.js';

export class JquerySkForm extends SkFormImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'form';
    }

    beforeRendered() {
        super.beforeRendered();
        this.attachStyleByPath(this.skTheme.basePath + '/jquery-theme.css', this.comp.querySelector('span[slot=fields]'));
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


}
